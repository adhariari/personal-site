  <!--==========================
  Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <a href="index.php"><img src="img/logo.png" alt="" title="" /></img></a>
        <!-- Uncomment below if you prefer to use a text logo -->
        <!--<h1><a href="#hero">Regna</a></h1>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu"><a href="index.php#hero">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li><a href="index.php#services">Feature</a></li>
          <li><a href="index.php#portfolio">Technical Design</a></li>
          <li><a href="index.php#team">Profile</a></li>
          <li><a href="peta.php">Peta KTA</a></li>
<!--           <li class="menu-has-children"><a href="#">Maps</a>
            <ul>
              <li><a href="petasra.php">Sumur Resapan Air (SRA)</a></li>
              <li><a href="petadpn.php">Dam Penahan (DPn)</a></li>
              <li><a href="petagp.php">Gully Plug (GP)</a></li>
            </ul>
          </li> -->
          <li class="menu-has-children"><a href="">Data Table</a>
          <ul>
              <li><a href="tablesra.php?kata=">Sumur Resapan Air (SRA)</a></li>
              <li><a href="tabledpn.php?kata=">Dam Penahan (DPn)</a></li>
              <li><a href="tablegp.php?kata=">Gully Plug (GP)</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->