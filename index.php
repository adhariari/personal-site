<?php
//date in mm/dd/yyyy format; or it can be in other formats as well
$birthDate = "06/05/1996";
//explode the date to get month, day and year
$birthDate = explode("/", $birthDate);
//get age from date or birthdate
$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
    ? ((date("Y") - $birthDate[2]) - 1)
    : (date("Y") - $birthDate[2]));
// echo "Age is:" . $age;
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Adhari | Online Resume</title>
	<meta name="description" content="Matresume is a material CV / Resume / Vcard / Portfolio HTML5 template by hencework." />
	<meta name="keywords" content="material design, resume, responsive template, cv, multipurpose, portfolio, html5 template, themeforest.net, bootstrap, html5, creative, landing page, sass, clean, design, modern, angular js," />
	<meta name="author" content="hencework"/>

	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">
	<!--CSS-->
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body id="body" data-ng-app="contactApp">

	<!--Preloader-->
	<div class="preloader-it">
		<div class="mdl-spinner mdl-js-spinner is-active is-upgraded" data-upgraded=",MaterialSpinner"><div class="mdl-spinner__layer mdl-spinner__layer-1"><div class="mdl-spinner__circle-clipper mdl-spinner__left"><div class="mdl-spinner__circle"></div></div><div class="mdl-spinner__gap-patch"><div class="mdl-spinner__circle"></div></div><div class="mdl-spinner__circle-clipper mdl-spinner__right"><div class="mdl-spinner__circle"></div></div></div><div class="mdl-spinner__layer mdl-spinner__layer-2"><div class="mdl-spinner__circle-clipper mdl-spinner__left"><div class="mdl-spinner__circle"></div></div><div class="mdl-spinner__gap-patch"><div class="mdl-spinner__circle"></div></div><div class="mdl-spinner__circle-clipper mdl-spinner__right"><div class="mdl-spinner__circle"></div></div></div><div class="mdl-spinner__layer mdl-spinner__layer-3"><div class="mdl-spinner__circle-clipper mdl-spinner__left"><div class="mdl-spinner__circle"></div></div><div class="mdl-spinner__gap-patch"><div class="mdl-spinner__circle"></div></div><div class="mdl-spinner__circle-clipper mdl-spinner__right"><div class="mdl-spinner__circle"></div></div></div><div class="mdl-spinner__layer mdl-spinner__layer-4"><div class="mdl-spinner__circle-clipper mdl-spinner__left"><div class="mdl-spinner__circle"></div></div><div class="mdl-spinner__gap-patch"><div class="mdl-spinner__circle"></div></div><div class="mdl-spinner__circle-clipper mdl-spinner__right"><div class="mdl-spinner__circle"></div></div></div></div>
	</div>
	<!--/Preloader-->

	<!--Main Wrapper-->
	<div class="main-wrapper">
		
		<!--Bg Image-->
		<div class="bg-struct bg-img"></div>
		<!--/Bg Image-->

		<div class="mdl-js-layout mdl-layout--fixed-header">

			<!--Top Header-->
			<header class="mdl-layout__header">
				<div class="mdl-layout__header-row mdl-scroll-spy-1">
					<!-- Title -->
					<a href="index.php"><span class="mdl-layout-title">Adhari</span></a>
					<div class="mdl-layout-spacer"></div>
					<ul class="nav mdl-navigation mdl-layout--large-screen-only">
						<li><a class="mdl-navigation__link" data-scroll href="#body">about</a></li>
						<li><a class="mdl-navigation__link" data-scroll href="#skills_sec">skills</a></li>
						<li><a class="mdl-navigation__link" data-scroll href="#portfolio_sec">portfolio</a></li>
						<li><a class="mdl-navigation__link" data-scroll href="#experience_sec">experience</a></li>
						<li><a class="mdl-navigation__link" data-scroll href="#education_sec">education</a></li>
						<li><a class="mdl-navigation__link" data-scroll href="#activity_sec">Activity</a></li>
						<li><a class="mdl-navigation__link" data-scroll href="#honor_sec">awards</a></li>
						<li><a class="mdl-navigation__link" data-scroll href="#contact_sec">contact</a></li>
					</ul>
					<!-- Right aligned menu below button -->
					<button id="demo-menu-lower-right"
					class="mdl-button mdl-js-button mdl-button--icon ver-more-btn">
					<i class="material-icons">more_vert</i>
				</button>

				<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
				data-mdl-for="demo-menu-lower-right">
				<li class="mdl-menu__item"><a href="CV-Adhari.pdf"><i class="zmdi zmdi-download font-red pr-10"></i>Download CV</a></li>
				<li class="mdl-menu__item"><a href="mailto:adhari.ariari@gmail.com"><i class="zmdi zmdi-email-open font-green pr-10"></i>Contact Me</a></li>
				<li class="mdl-menu__item"><a href="https://wa.me/6285779627790" target="_blank"><i class="zmdi zmdi-phone  font-blue  pr-10"></i>WhatsApp</a></li>
			</ul>
		</div>
	</header>
	<!--/Top Header-->

	<!--Left Sidebar-->
	<div class="mdl-layout__drawer">
		<div class="nicescroll-bar">
			<div class="drawer-profile-wrap">
				<div class="candidate-img-drawer mt-25 mb-20"></div>
				<span class="candidate-name block mb-10 text-center">adhari</span>
				<ul class="social-icons  mb-30">
					<li>
						<a class="facebook-link" href="https://web.facebook.com/sayaadhari" target="_blank">
							<i id="tt11" class="zmdi zmdi-facebook"></i>
							<div class="mdl-tooltip" data-mdl-for="tt11">
								facebook
							</div>
						</a>
					</li>
					<li>
						<a class="twitter-link" href="https://twitter.com/ariganesha" target="_blank">
							<i id="tt12" class="zmdi zmdi-twitter"></i>
							<div class="mdl-tooltip" data-mdl-for="tt12">
								twitter
							</div>
						</a>
					</li>
					<li>
						<a class="linkedin-link" href="https://www.linkedin.com/in/ariadhari/" target="_blank">
							<i id="tt13" class="zmdi zmdi-linkedin"></i>
							<div class="mdl-tooltip" data-mdl-for="tt13">
								linkedin
							</div>
						</a>
					</li>
					<li>
						<a class="instagram-link" href="https://www.instagram.com/adhariarii/" target="_blank">
							<i id="tt15" class="zmdi zmdi-instagram"></i>
							<div class="mdl-tooltip" data-mdl-for="tt15">
								instagram
							</div>
						</a>
					</li>
				</ul>
			</div>
			<div class="mdl-scroll-spy-2">
				<ul class="mdl-navigation">
					<li>
						<a class="mdl-navigation__link border-top-sep" data-scroll href="#body">
							<i class="zmdi zmdi-border-color pr-15"></i>
							<span class="font-capitalize">about</span>
						</a>
					</li>	
					<li>
						<a class="mdl-navigation__link border-top-sep" data-scroll href="#skills_sec">
							<i class="zmdi zmdi-cutlery pr-15"></i>
							<span class="font-capitalize">skills</span>
						</a>
					</li>
					<li>
						<a class="mdl-navigation__link border-top-sep" data-scroll href="#portfolio_sec">
							<i class="zmdi zmdi-case pr-15"></i>
							<span class="font-capitalize">portfolio</span>
						</a>
					</li>	
					<li>
						<a class="mdl-navigation__link border-top-sep" data-scroll href="#experience_sec">
							<i class="zmdi zmdi-shield-check pr-15"></i>
							<span class="font-capitalize">experience</span>
						</a>
					</li>	
					<li>
						<a class="mdl-navigation__link border-top-sep" data-scroll href="#education_sec">
							<i class="zmdi zmdi-library pr-15"></i>
							<span class="font-capitalize">education</span>
						</a>
					</li>	

					<li>
						<a class="mdl-navigation__link border-top-sep"  data-scroll href="#activity_sec"">
							<i class="zmdi zmdi-tag-more pr-15"></i>
							<span class="font-capitalize">activity</span>
						</a>
					</li>

					<li>
						<a class="mdl-navigation__link border-top-sep" data-scroll href="#honor_sec">
							<i class="zmdi zmdi-bookmark pr-15"></i>
							<span class="font-capitalize">awards</span>
						</a>
					</li>	

					<li>
						<a class="mdl-navigation__link border-top-sep border-bottom-sep" 	data-scroll  href="#contact_sec">
							<i class="zmdi zmdi-email pr-15"></i>
							<span class="font-capitalize">contact</span>
						</a>
					</li>	
				</ul>
			</div>
			<div class="drawer-footer mt-50 mb-30 text-center">
				<p class="font-12 mt-10">Adhari &copy; 2019.</p>
			</div>
		</div>
	</div>
	<!--/Left Sidebar-->

	<!--Main Content-->
	<div class="main-content relative">
		<div class="container">

			<!--About Sec-->
			<section class="about-sec mt-180 mt-sm-120  mb-30">
				<div class="row">
					<div class="col-lg-12">
						<div class="mdl-card mdl-shadow--2dp">
							<div class="row">
								<div class="col-md-5 col-xs-12 mb-30">
									<div class="candidate-img mb-35"></div>
									<ul class="social-icons">
										<li>
											<a class="facebook-link" href="https://web.facebook.com/sayaadhari" target="_blank">
												<i id="tt11" class="zmdi zmdi-facebook"></i>
												<div class="mdl-tooltip" data-mdl-for="tt11">
													facebook
												</div>
											</a>
										</li>
										<li>
											<a class="twitter-link" href="https://twitter.com/ariganesha" target="_blank">
												<i id="tt12" class="zmdi zmdi-twitter"></i>
												<div class="mdl-tooltip" data-mdl-for="tt12">
													twitter
												</div>
											</a>
										</li>
										<li>
											<a class="linkedin-link" href="https://www.linkedin.com/in/ariadhari/" target="_blank">
												<i id="tt13" class="zmdi zmdi-linkedin"></i>
												<div class="mdl-tooltip" data-mdl-for="tt13">
													linkedin
												</div>
											</a>
										</li>
										<li>
											<a class="instagram-link" href="https://www.instagram.com/adhariarii/" target="_blank">
												<i id="tt15" class="zmdi zmdi-instagram"></i>
												<div class="mdl-tooltip" data-mdl-for="tt15">
													instagram
												</div>
											</a>
										</li>
									</ul>
								</div>
								<div class="col-md-7 col-xs-12">
									<div class="info-wrap">
										<h1>adhari</h1>
										<h5 class="mt-20 font-grey">Web Developer</h5>
										<div class="mt-30">
											<a id="download_cv" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  bg-green font-white mr-10" href="CV-Adhari.pdf" download>download cv</a>
											<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect bg-blue font-white" href="#contact_sec" data-scroll>contact</a>
										</div>
									</div>
									<ul class="profile-wrap mt-50">
										<li>
											<div class="profile-title">Age</div>
											<div class="profile-desc"><?php echo $age; ?></div>
										</li>
										<li>
											<div class="profile-title">Nationality</div>
											<div class="profile-desc">Indonesia</div>
										</li>									
										<li>
											<div class="profile-title">address</div>
											<div class="profile-desc">
												Jl. Raya RTM Kelapa Dua Depok
											</div>
										</li>
										<li>
											<div class="profile-title">email</div>
											<div class="profile-desc">
												adhari.ariari@gmail.com
											</div>
										</li>
										<li>
											<div class="profile-title">phone</div>
											<div class="profile-desc">
												+62 857 7962 7790
											</div>
										</li>
										<li>
											<div class="profile-title">freelance</div>
											<div class="profile-desc relative">Available
											</div>

										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</section>
			<!--/About Sec-->		

			<!--Text Sec-->
			<h2 class="mb-30">About me</h2>
			<section class="about-sec">
				<div class="row">
					<div class="col-lg-12">
						<div class="mdl-card mdl-shadow--2dp">
							<p style="text-align:justify">Graduated from Gunadarma University,
							majoring in Information System with GPA 3.75 out 4.00. Experienced as web developer since 2016 and still continues with interest in front-end and back-end website. Well-skilled in web development mostly using PHP Native or Laravel Framework, analysis, testing, troubleshooting, technical support, creative design, and hardware or software installation. Eager to learn, discipline, fast learner, good team work and hard worker.</p>
						</div>
					</div>
				</div>
			</section>
			<!--Text Sec-->

			<!--Skills Sec-->
			<section id="skills_sec" class="skills-sec sec-pad-top-sm">
				<div class="row">
					<div class="col-sm-6 mb-30">
						<h2 class="mb-30">Technical Skills</h2>
						<div class="mdl-card mdl-shadow--2dp">
							<div class="mb-30">
								<span class="progress-cat">PHP Native/Framework (Laravel) </span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap red-bar">
											<span data-width="90"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-30">
								<span class="progress-cat">HTML5 & CSS</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap blue-bar">
											<span data-width="87"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-30">
								<span class="progress-cat">Database (MySQL & Oracle)</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap green-bar">
											<span data-width="85"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-30">
								<span class="progress-cat">Javascript</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap yellow-bar">
											<span data-width="75"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-30">
								<span class="progress-cat">Networking</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap red-bar">
											<span data-width="78"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 mb-30">
						<h2 class="mb-30">Other Skills</h2>
						<div class="mdl-card mdl-shadow--2dp">
							<div class="mb-30">
								<span class="progress-cat">Git Version Control</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap red-bar">
											<span data-width="85"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-30">
								<span class="progress-cat">Adobe Photoshop</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap blue-bar">
											<span data-width="95"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-30">
								<span class="progress-cat">Adobe Illustrator</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap green-bar">
											<span data-width="80"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-30">
								<span class="progress-cat">Adobe Premiere</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap yellow-bar">
											<span data-width="85"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-30">
								<span class="progress-cat">Project Management</span>
								<div class="progress-bar-graph">
									<div class="progress-bar-wrap">
										<div class="bar-wrap red-bar">
											<span data-width="81"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/Skills Sec-->

			<!--Profile Sec-->
			<section id="profile_sec" class="profile-sec sec-pad-top-sm">
				<h2 class="mb-30">what i do</h2>
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-12 mb-30">
						<div class="mdl-card mdl-shadow--2dp">
							<i class="zmdi zmdi-format-color-fill font-blue profile-icon"></i>
							<h4 class="mb-15">Designing</h4>
							<p>Creating blue print layout with responsive design.</p>

						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 mb-30">
						<div class="mdl-card mdl-shadow--2dp">
							<i class="zmdi zmdi-format-color-text font-green profile-icon"></i>
							<h4 class="mb-15">coding</h4>										
							<p>Make the concept happens with powerful and clean code.</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 mb-30">
						<div class="mdl-card mdl-shadow--2dp">
							<i class="zmdi zmdi-comments font-yellow profile-icon"></i>
							<h4 class="mb-15">Testing</h4>
							<p>Check all features work perfectly for end users.&nbsp;&nbsp;&nbsp;</p>

						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 mb-30">
						<div class="mdl-card mdl-shadow--2dp">
							<i class="zmdi zmdi-grain font-red profile-icon"></i>
							<h4 class="mb-15">Maintaining</h4>
							<p>Great app publishing and keep maintainable.&nbsp;&nbsp;&nbsp;&nbsp;</p>

						</div>
					</div>
				</div>
			</section>
			<!--/Profile Sec-->

			<!--Portfolio Sec-->
			<section id="portfolio_sec" class="portfolio-sec sec-pad-top-sm">
				<div class="mb-15">
					<h2 class="pull-left float-none-xs">portfolio</h2>
					<div class="filter-wrap pull-right float-none-xs">
						<!-- Portfolio Filters --> 
									<ul id="filters">
										<li><a id="all" href="#" data-filter="*" class="active">all</a></li>	
										<li><a href="#" data-filter=".freelance">Freelance</a></li>
										<!-- <li><a href="#" data-filter=".design">design</a></li>
										<li><a href="#" data-filter=".photography">photo</a></li>
										<li><a href="#" data-filter=".web">web</a></li> -->
									</ul>
									<!--/Portfolio Filters -->
									<div class="clearfix"></div>
								</div> 
								<div class="clearfix"></div> 
							</div>  
							<div class="gallery-wrap mb-30">
								<div class="portfolio-wrap project-gallery">
									<ul id="portfolio" class="portf auto-construct  project-gallery" data-col="3">
										<li  class="item mdl-card mdl-shadow--2dp pa-0">
											<div class="light-img-wrap mdl-card__title pa-0">
												<img class="img-responsive" src="img/gallery1.jpg"  alt="Image description" />
												<div class="light-img-overlay"></div>
											</div>	
											<span class="bottom-links mdl-card__actions" style="text-align: center;">
												<a  href="http://contrasnews.com" style="padding: 0px;" target="_blank">Contras News</a>
											</span>
										</li>
										<li class="item mdl-card mdl-shadow--2dp pa-0">
											<div class="light-img-wrap mdl-card__title pa-0">
												<img class="img-responsive" src="img/gallery2.jpg"  alt="Image description" />
												<div class="light-img-overlay"></div>
											</div>
											<span class="bottom-links mdl-card__actions" style="text-align: center;">
												<a  href="http://www.rekan.web.id" style="padding: 0px;" target="_blank">Rekan Konstruksi</a>
											</span>
										</li>
										<li class="item mdl-card mdl-shadow--2dp pa-0 freelance">
											<div class="light-img-wrap mdl-card__title pa-0">
												<img class="img-responsive" src="img/gallery3.jpg"  alt="Image description" />
												<div class="light-img-overlay"></div>
											</div>
											<span class="bottom-links mdl-card__actions" style="text-align: center;">
												<a  href="portofolio/sigkta" style="padding: 0px;" target="_blank">SIG Bangunan KTA</a>
											</span>
										</li>
										<li class="item mdl-card mdl-shadow--2dp pa-0 freelance">
											<div class="light-img-wrap mdl-card__title pa-0">
												<img class="img-responsive" src="img/gallery4.jpg"  alt="Image description" />
												<div class="light-img-overlay"></div>
											</div>
											<span class="bottom-links mdl-card__actions" style="text-align: center;">
												<a  href="portofolio/sermod" style="padding: 0px; font-size: 20px;" target="_blank">Service Motor on Delivery</a>
											</span>
										</li>
										
										<li class="item mdl-card mdl-shadow--2dp pa-0 freelance">
											<div class="light-img-wrap mdl-card__title pa-0">
												<img class="img-responsive" src="img/gallery5.jpg"  alt="Image description" />
												<div class="light-img-overlay"></div>
											</div>
											<span class="bottom-links mdl-card__actions" style="text-align: center;">
												<a  href="portofolio/poster" style="padding: 0px;" target="_blank">Negeri Poster</a>
											</span>
										</li>
										<li class="item mdl-card mdl-shadow--2dp pa-0 freelance">
											<div class="light-img-wrap mdl-card__title pa-0">
												<img class="img-responsive" src="img/gallery6.jpg"  alt="Image description" />
												<div class="light-img-overlay"></div>
											</div>
											<span class="bottom-links mdl-card__actions" style="text-align: center;">
												<a  href="portofolio/event" style="padding: 0px; font-size: 16px;" target="_blank">Application Management Event</a>
											</span>
										</li>
										
									</ul>
									<!-- Hidden video div -->
									<div style="display:none;" id="video1">
										<video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
											<source src="video/video1.mp4" type="video/webm">
												<source src="video/video1.webm" type="video/webm">
													Your browser does not support HTML5 video.
												</video>
											</div>
										</div>
									</div>
								</section>
								<!--/Portfolio Sec-->

								<!--Interest Sec-->
								<section id="interest_sec" class="interest-sec sec-pad-top-sm">
									<h2 class="mb-30">interests</h2>
									<div class="row">
										<div class="col-md-2 col-sm-4 col-xs-6 mb-30">
											<div class="mdl-card mdl-shadow--2dp text-center pa-20 pt-30 pb-30">
												<i class="zmdi zmdi-bike"></i>
												<span>bicycling</span>
											</div>
										</div>	
										<div class="col-md-2 col-sm-4 col-xs-6 mb-30">
											<div class="mdl-card mdl-shadow--2dp text-center pa-20 pt-30 pb-30">
												<i class="zmdi zmdi-smartphone-ring"></i>
												<span>gaming</span>
											</div>
										</div>	
										<div class="col-md-2 col-sm-4 col-xs-6 mb-30">
											<div class="mdl-card mdl-shadow--2dp text-center pa-20 pt-30 pb-30">
												<i class="zmdi zmdi-camera"></i>
												<span>photography</span>
											</div>
										</div>
										<div class="col-md-2 col-sm-4 col-xs-6 mb-30">
											<div class="mdl-card mdl-shadow--2dp text-center pa-20 pt-30 pb-30">
												<i class="zmdi zmdi-library"></i>
												<span>reading</span>
											</div>
										</div>
										<div class="col-md-2 col-sm-4 col-xs-6 mb-30">
											<div class="mdl-card mdl-shadow--2dp text-center pa-20 pt-30 pb-30">
												<i class="zmdi zmdi-airplane"></i>
												<span>traveling</span>
											</div>
										</div>
										<div class="col-md-2 col-sm-4 col-xs-6 mb-30">
											<div class="mdl-card mdl-shadow--2dp text-center pa-20 pt-30 pb-30">
												<i class="zmdi zmdi-dribbble"></i>
												<span>sports</span>
											</div>
										</div>
									</div>
								</section>
								<!--/Interest Sec-->

								<!--Experience Sec-->
								<section id="experience_sec" class="experience-sec sec-pad-top-sm">
									<h2 class="mb-30">Working Experience</h2>
									<div class="timeline-wrap  overflow-hide mb-30">
										<ul class="timeline">
											<li>
												<div class="timeline-badge">
													<i class="zmdi zmdi-case font-red"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-red">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/MidasLogo.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">IT Application and EOD Support Consultant</h4>
																<span class="duration mb-5">Nov 2018 - Oct 2019</span>
																<span class="institution">PT. Midas Daya Teknologi<br>IT Vendor & Consulting</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">PT Midas Daya Teknologi is an IT Consulting, Product and Services company. Started by professionals who have deep experience in core banking transformation, development, enterprise architecture, infrastructure design and management, big data solutions and project management.</p>
														<div class="mt-15 responsibility">
															<p>Responsibility (Indonesia Exim Bank Client):</p>
															<ul>
																<li>• As a technical engineer to enhance or maintain core banking (Oracle Flexcube)</li>
																<li>• Coordinating technical and functional aspect between bank & Oracle team (Indonesia & India)</li>
																<li>• Managing core banking database and weblogic deployment, do patching from backend using PL/SQL</li>
																<li>• Solving more over 59 operational issues in production / development environment</li>
																<li>• Implementing CL Account Branch Transfer, Collateral and SWIFT Message as core banking enhancement</li>															
															</ul>
														</div>
													</div>
												</div>
											</li>
											<li class="timeline-inverted">
												<div class="timeline-badge">
													<i class="zmdi zmdi-case font-green"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-green">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/img-contras.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Web Programmer</h4>
																<span class="duration mb-5">Jan 2018 - Jan 2019</span>
																<span class="institution">Contras News<br>Media Portal</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">Contrasnews.com is a news website thats owned by Kombes. Pol. Argo Yuwono as Kabidhumas Polda Metro Jaya. As a web programmer my main task is for develop website like create website, hosting to web hosting, setting layout for news article or widget and optimizing SEO.</p>
														<div class="mt-15 responsibility">
															<p>Responsibility :</p>
															<ul>
																<li>• Responsible to create website using CMS </li>
																<li>• Hosting the website to Qwords web hosting</li>
																<li>• Setting layout for news article and widget</li>
																<li>• Optimizing SEO</li>
															</ul>
														</div>
													</div>
												</div>
											</li>
											<li>
												<div class="timeline-badge">
													<i class="zmdi zmdi-case font-blue"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-blue">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/rekan.jpg" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Project Web Developer</h4>
																<span class="duration mb-5">Sep 2018 - Dec 2018</span>
																<span class="institution">Rekan Konstruksi<br>Construction Company</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">Rekan Konstruksi is a small company focused in Construction building, renovation and installation. This company needs a website for potential customers to find out information about estimated construction / renovation costs.</p>
														<div class="mt-15 responsibility">
															<p>Responsibility :</p>
															<ul>
																<li>• Responsible to create website using PHP Native for any back-end process (Front-end and Admin Dashboard)</li>
																<li>• Hosting the website to Idcloudhost web hosting</li>
																<li>• Use Bootstrap 4 framework CSS to make beautiful and responsive website</li>
																<li>• Use MySQL Database for any CRUD process</li>
																<li>• Use tawk.to interactive chat API to serve customers</li>
															</ul>
														</div>
													</div>
												</div>
											</li>												
											<li class="timeline-inverted">
												<div class="timeline-badge">
													<i class="zmdi zmdi-case font-blue"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-blue">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/img-web.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Full-Stack Web Developer</h4>
																<span class="duration mb-5">Sep 2016 - Nov 2018</span>
																<span class="institution">Self Employed (Freelance)<br>IT Consulting</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">When i was in college, i got task to write scientific paper and i must made an application for my scientific paper so i decided to create website and then i'm very interested with Web Programming because i finished my scientific paper very well maybe i think this is my passion in web programming. So after that, i start develop some website for freelance or non-profit use.</p>
														<div class="mt-15 responsibility">
															<p>Responsibility :</p>
															<ul>
																<li>• Learning something new about web development.</li>
																<li>• Make client's idea dream come true.</li>
																<li>• Design web based on client needs.</li>
															</ul>
															<br>
															<ul>
															<p>Work on Products :</p>																
																<li>• Sistem Informasi Geografis Bangunan KTA</li>
																<li>• Service Motor on Delivery</li>
																<li>• Negeri Poster</li>
																<li>• Application Event Management</li>
															</ul>
														</div>
													</div>
												</div>
											</li>																					
											<li>
												<div class="timeline-badge">
													<i class="zmdi zmdi-case font-yellow"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-yellow">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/ilab.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Chief Assistant of Operational Division</h4>
																<span class="duration mb-5">Mar 2016 - Sep 2018</span>
																<span class="institution">ilab Gunadarma University<br>Laboratory University</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">Integrated Laboratory also known as iLab is the largest laboratory in Gunadarma University. As a chief assistant of operational division my main task is responsible for handling laboratory Campus H daily operational and administrative activities also providing for technical troubleshooting issues & IT Support. Responsible for recruiting, training and commanding of field assistants.</p>
														<div class="mt-15 responsibility">
															<p>Responsibility :</p>
															<ul>
																<li>• Responsible for laboratory and Campus H daily operational and administrative activities.</li>
																<li>• Commanding and supervising a team of 40 field assistants</li>
																<li>• Setting goals and objectives, distributing workload, and maintaining team's working standards.</li>
																<li>• Responsible for recruitment, training, and development of field assistants.</li>
																<li>• Providing support and analysis functions for technical troubleshooting issues.</li>
															</ul>
														</div>
													</div>
												</div>
											</li>
											<li class="clearfix no-float"></li>
										</ul>
									</div>
								</section>
								<!--/Experience Sec-->

								<!--Education Sec-->
								<section id="education_sec" class="education-sec sec-pad-top-sm">
									<h2 class="mb-30 ">education</h2>
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12 mb-30">
											<div class="mdl-card mdl-shadow--2dp">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/img-gunadarma.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Bachelor’s Degree of Information System <br>(Computer Science)</h4>
																<span class="duration mb-5">2014 - 2018</span><br>
																<span class="institution">Gunadarma University</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">Universitas Gunadarma (Gunadarma University) is a higher education institution located in the suburban setting of the large city of Depok , West Java. In campus life, I am always exploring about code inside and outside the college. I am study for 4 years in Gunadarma University with Undergraduate Thesis Title "Sistem Informasi Geografis Persebaran Bangunan Konservasi Tanah Dan Air Di Kabupaten Bandung Sub DAS Cisangkuy Das Citarum Berbasis Web".</p>
														<div class="mt-15 responsibility">
													</div>
											</div>
										</div>
									</div>
										<div class="col-md-6 col-sm-6 col-xs-12 mb-30">
											<div class="mdl-card mdl-shadow--2dp">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/img-sma.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Social Sciences</h4>
																<span class="duration mb-5">2011 - 2014</span><br>
																<span class="institution">SMA Negeri 106 Jakarta</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify"><br><br>SMA 106 Jakarta was inaugurated on November 20, 1990, by the Governor of DKI Jakarta Mr. Wiyogo Atmodarminto. This school is located at Jalan Gandaria I, Pekayon, Pasar Rebo, East Jakarta. I am study social science major for 3 years in SMAN 106 Jakarta<br><br><br><br><br></p>
													</div>
											</div>
										</div>
									</div>
								</section>
								<!--/Education Sec-->

								<!--Activity Sec-->
								<section id="activity_sec" class="profile-sec sec-pad-top-sm">
									<h2 class="mb-30">Organization & Other Activity</h2>
									<div class="timeline-wrap  overflow-hide mb-30">
										<ul class="timeline">

											<li>
												<div class="timeline-badge bg-yellow no-icon"></div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-yellow">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/img-bem.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Staff of Academic Department</h4>
																<span class="duration mb-5">2015 - 2016</span>
																<span class="institution">BEM FIKTI<br>Gunadarma University</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">When i was in BEM FIKTI Gunadarma, i became a project officer of the seminar who held in Campus J1 Kalimalang. The topic of the seminar is about Windows 10 with Mr. Irving Hutagalung as the speakers from Microsoft Indonesia. Also became as coordinator of Public Relations, Publications and Documentation of the biggest event technology Techno Fair 2016 which held Seminars, Talkshows, Workshops and Exhibitions in 4 domicile Gunadarma University</p>
													</div>
												</div>
											</li>

											<li class="timeline-inverted">
												<div class="timeline-badge bg-red no-icon"></div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-red">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/img-iak.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Student</h4>
																<span class="duration mb-5">Jul 2017</span>
																<span class="institution">Indonesia Android Kejar 3.0<br>Google Developers</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">Indonesia Android Kejar (IAK) is a program that initiated by Google Developers to support Indonesian Developers for developing mobile application through udacity online learning and offline meetings with the local community. Became a student with intermediate level</p>
													</div>
												</div>
											</li>

											<li>
												<div class="timeline-badge bg-blue no-icon"></div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-blue">
													<div class="row">
														<div class="col-sm-4">
															<img src="img/img-iak.png" width="96" height="96">
														</div>
														<div class="col-sm-8">
															<div class="timeline-heading">
																<h4 class="mb-10">Student</h4>
																<span class="duration mb-5">Sep 2016</span>
																<span class="institution">Indonesia Android Kejar 2.0<br>Google Developers</span>
															</div>
														</div>
													</div>
													<div class="timeline-body">
														<p class="mt-25" style="text-align:justify">Indonesia Android Kejar (IAK) is a program that initiated by Google Developers to support Indonesian Developers for developing mobile application through udacity online learning and offline meetings with the local community. Became a student with beginner level</p>
													</div>
												</div>
											</li>

											<li class="clearfix no-float"></li>
										</ul>
									</div>
								</section>
								<!--/Activity Sec-->

								<!--Certificate Sec-->
								<section id="honor_sec" class="education-sec sec-pad-top-sm">
									<h2 class="mb-30">certificates, awards & honors</h2>
									<div class="timeline-wrap overflow-hide mb-30">
										<ul class="timeline">
											<li>
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-red"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-red">
													<div class="timeline-heading">
														<h4 class="mb-10">Brilliant English Course </h4>
														<span class="duration mb-5">Oct 2018 – Present • License 16722/Brilli EC/2018</span>
														<span class="institution">Brilliant English Course Kampung Inggris Pare</span>
													</div>
												</div>
											</li>

											<li class="timeline-inverted">
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-blue"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-blue">
													<div class="timeline-heading">
														<h4 class="mb-10">C# For Beginner</h4>
														<span class="duration mb-5">Mar 2018 – Present • License 2110249210216146</span>
														<span class="institution">LePKom Univ. Gunadarma</span>
													</div>
												</div>
											</li>

											<li>
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-green"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-green">
													<div class="timeline-heading">
														<h4 class="mb-10">C# for Intermediate</h4>
														<span class="duration mb-5">Mar 2018 – Present • License 3112003250816246</span>
														<span class="institution">LePKom Univ. Gunadarma</span>
													</div>
												</div>
											</li>

											<li class="timeline-inverted">
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-yellow"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp pt-30 pb-30 border-top-yellow">
													<div class="timeline-heading">
														<h4 class="mb-10">Designing Shopping Cart</h4>
														<span class="duration mb-5">Mar 2018 – Present • License 1703011018317201</span>
														<span class="institution">LePKom Univ. Gunadarma</span>
													</div>
												</div>
											</li>

											<li>
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-red"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-red">
													<div class="timeline-heading">
														<h4 class="mb-10">Fundamental ERP</h4>
														<span class="duration mb-5">Jan 2018 – Present • License 1020164280215146</span>
														<span class="institution">LePKom Univ. Gunadarma</span>
													</div>
												</div>
											</li>

											<li class="timeline-inverted">
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-blue"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-blue">
													<div class="timeline-heading">
														<h4 class="mb-10">Fundamental Web Programming</h4>
														<span class="duration mb-5">Jan 2018 – Present • License 1030008250715246</span>
														<span class="institution">LePKom Univ. Gunadarma</span>
													</div>
												</div>
											</li>

											<li>
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-green"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-green">
													<div class="timeline-heading">
														<h4 class="mb-10">Introduction to Microsoft Dynamics NAV C/Side Programming</h4>
														<span class="duration mb-5">Jan 2018 – Present • License 3040039280216146</span>
														<span class="institution">LePKom Univ. Gunadarma</span>
													</div>
												</div>
											</li>

											<li class="timeline-inverted">
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-yellow"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-yellow">
													<div class="timeline-heading">
														<h4 class="mb-10">Microsoft Dynamics NAV Project</h4>
														<span class="duration mb-5">Jan 2018 – Present • License 4041728180118746</span>
														<span class="institution">LePKom Univ. Gunadarma</span>
													</div>
												</div>
											</li>

											<li>
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-red"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-red">
													<div class="timeline-heading">
														<h4 class="mb-10">Microsoft Dynamics NAV for Beginner</h4>
														<span class="duration mb-5">Jan 2018 – Present • License 2040031210216146</span>
														<span class="institution">LePKom Univ. Gunadarma</span>
													</div>
												</div>
											</li>

											<li class="timeline-inverted">
												<div class="timeline-badge">
													<i class="zmdi zmdi-assignment font-blue"></i>
												</div>
												<div class="timeline-panel mdl-card mdl-shadow--2dp  pt-30 pb-30 border-top-blue">
													<div class="timeline-heading">
														<h4 class="mb-10">TOEFL Course</h4>
														<span class="duration mb-5">Dec 2016 • Score 523</span>
														<span class="institution">Sekolah TOEFL</span>
													</div>
												</div>
											</li>											
											<li class="clearfix no-float"></li>
										</ul>
									</div>
								</section>
								<!--/Certificate Sec-->

								

								<!--Blog Sec-->
						<!-- <section id="blog_sec" class="blog-sec sec-pad-top-sm">
							<h2 class="mb-30">blog</h2>
							<div class="row">
								<div class="col-sm-4 mb-30">
									<div class="mdl-card mdl-shadow--2dp pa-0">
										<div class="mdl-card__title pa-0">
											<div class="blog-img blog-1"></div>
										</div>
										<div class="mdl-card__supporting-text relative">
											<span class="blog-cat">travel</span>
											<a href="youtube-blog-post.html">
												<h4 class="mt-15 mb-20">Fly High with youtube</h4>
											</a>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eleifend lacinia...</p>
											<a href="youtube-blog-post.html" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-red mdl-shadow--8dp">
												<i class="zmdi zmdi-youtube-play"></i>
											</a>
										</div>
										<div class="mdl-card__actions mdl-card--border">
											<span class="blog-post-date inline-block">21.1.17</span>
											<div class="mdl-layout-spacer"></div>
											<button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mr-5">
												<i class="zmdi zmdi-favorite"></i>
											</button>	
											<button id ="share_menu_1" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
												<i class="zmdi zmdi-share"></i>
											</button>
											<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
												data-mdl-for="share_menu_1">
											  <li class="mdl-menu__item">Facebook</li>
											  <li class="mdl-menu__item">Twitter</li>
											  <li class="mdl-menu__item">LinkedIn</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-sm-4 mb-30">
									<div class="mdl-card mdl-shadow--2dp pa-0">
										<div class="mdl-card__title pa-0">
											<div class="blog-img blog-2"></div>
										</div>
										<div class="mdl-card__supporting-text relative">
											<span class="blog-cat">photography</span>
											<a href="image-blog-post.html">
												<h4 class="mt-15 mb-20">Iceland live in images</h4>
											</a>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eleifend lacinia...</p>
											<a href="image-blog-post.html" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue mdl-shadow--8dp">
												<i class="zmdi zmdi-image-o"></i>
											</a>
										</div>
										<div class="mdl-card__actions mdl-card--border">
											<span class="blog-post-date inline-block">15.1.17</span>
											<div class="mdl-layout-spacer"></div>
											<button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mr-5">
												<i class="zmdi zmdi-favorite"></i>
											</button>	
											<button id ="share_menu_2" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
												<i class="zmdi zmdi-share"></i>
											</button>
											<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
												data-mdl-for="share_menu_2">
											  <li class="mdl-menu__item">Facebook</li>
											  <li class="mdl-menu__item">Twitter</li>
											  <li class="mdl-menu__item">LinkedIn</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-sm-4 mb-30">
									<div class="mdl-card mdl-shadow--2dp pa-0">
										<div class="mdl-card__title pa-0">
											<div class="blog-img blog-3"></div>
										</div>
										<div class="mdl-card__supporting-text relative">
											<span class="blog-cat">science</span>
											<a href="gallery-blog-post.html">
												<h4 class="mt-15 mb-20">Grand canyon mysteries</h4>
											</a>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eleifend lacinia...</p>
											<a href="gallery-blog-post.html" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-green mdl-shadow--8dp">
												<i class="zmdi zmdi-collection-image-o"></i>
											</a>
										</div>
										<div class="mdl-card__actions mdl-card--border">
											<span class="blog-post-date inline-block">9.1.17</span>
											<div class="mdl-layout-spacer"></div>
											<button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mr-5">
												<i class="zmdi zmdi-favorite"></i>
											</button>	
											<button id ="share_menu_3" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
												<i class="zmdi zmdi-share"></i>
											</button>
											<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
												data-mdl-for="share_menu_3">
											  <li class="mdl-menu__item">Facebook</li>
											  <li class="mdl-menu__item">Twitter</li>
											  <li class="mdl-menu__item">LinkedIn</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="text-center mt-20 mb-30">
								<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  margin-lr-auto view-more" href="blog-list.html">view all</a>
							</div>	
						</section>	 -->
						<!--/Blog Sec-->
						
						<!--References Sec-->
						<!-- <section id="references_sec" class="reference-sec sec-pad-top-sm">
							<h2 class="mb-30">testimonial</h2>
							<div class="row">
								<div class="col-sm-12 mb-30">
									<div class="mdl-card mdl-shadow--2dp border-top-yellow pa-35">
										<div class="testimonial-carousel">
											<div>
												<blockquote>"Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto."</blockquote>
												<span class="ref-name block mb-5 mt-20">john doe</span>
												<span class="ref-desgn block">Lead Designer in Droffox</span>
											</div>
											<div>
												<blockquote>"Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto."</blockquote>
												<span class="ref-name block mb-5 mt-20">Shone doe</span>
												<span class="ref-desgn block">Lead Designer in Fakebook</span>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						
						</section> -->
						<!--/References Sec-->
						
						<!--Client Sec-->
<!-- 						<section id="client_sec" class="client-sec sec-pad-top-sm">
							<div class="row">
								<div class="col-sm-12 mb-30">
									<div class="client-carousel">
										<img src="img/client1.png" alt="client">
										<img src="img/client2.png" alt="client">
										<img src="img/client3.png" alt="client">
										<img src="img/client4.png" alt="client">
										<img src="img/client5.png" alt="client">
										<img src="img/client6.png" alt="client">
									</div>
								</div>
							</div>
						</section> -->
						<!--/Client Sec-->

						<!--Price Sec-->
						<!-- <section id="price_sec" class="price-sec sec-pad-top-sm">
							<h2 class="mb-30">pricing</h2>
							<div class="row">
								<div class="col-sm-4 text-center mb-30">
									<div class="mdl-card mdl-shadow--2dp pa-0">
										<div class="mdl-card__title pa-0">
											<h5 class="block pt-25 pb-20">basic</h5>
											<div class="price-bg">
												<span class="panel-price block font-blue"><span class="pricing-dolor">$</span>49</span>
												<span class="pricing-plan block pt-10">per month</span>
											</div>	
										</div>
										<div class="mdl-card__supporting-text text-center pa-0">
											<ul class="list-group mb-0 text-center">
												<li class="list-group-item">1 website</li>
												<li class="list-group-item">30 GB Storage</li>
												<li class="list-group-item">Unlimited bandwidth</li>
												<li class="list-group-item">Free email included</li>
											</ul>
										</div>
										<div class="mdl-card__actions pt-30 pb-30">
											<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  bg-green font-white" href="#">buy now</a>
										</div>
									</div>
								</div>
								
								
								<div class="col-sm-4 text-center mb-30">
									<div class="mdl-card mdl-shadow--2dp pa-0">
										<div class="mdl-card__title pa-0">
											<h5 class="block pt-25 pb-20">standard</h5>
											<div class="price-bg">
												<span class="panel-price block font-blue"><span class="pricing-dolor">$</span>99</span>
												<span class="pricing-plan block pt-10">per month</span>
											</div>	
										</div>
										<div class="mdl-card__supporting-text text-center pa-0">
											<ul class="list-group mb-0 text-center">
												<li class="list-group-item">1 website</li>
												<li class="list-group-item">100 GB storage</li>
												<li class="list-group-item">Unlimited bandwidth</li>
												<li class="list-group-item">Free domain & email</li>
											</ul>
										</div>
										<div class="mdl-card__actions pt-30 pb-30">
											<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  bg-green font-white" href="#">buy now</a>
										</div>
									</div>
								</div>
								
								
								<div class="col-sm-4 text-center mb-30">
									<div class="mdl-card mdl-shadow--2dp pa-0">
										<div class="mdl-card__title pa-0">
											<h5 class="block pt-25 pb-20">business</h5>
											<div class="price-bg">
												<span class="panel-price block font-blue"><span class="pricing-dolor">$</span>199</span>
												<span class="pricing-plan block pt-10">per month</span>
											</div>	
										</div>
										<div class="mdl-card__supporting-text text-center pa-0">
											<ul class="list-group mb-0 text-center">
												<li class="list-group-item">Unlimited websites</li>
												<li class="list-group-item">Unlimited storage</li>
												<li class="list-group-item">Unlimited bandwidth</li>
												<li class="list-group-item">Free domain & email</li>
											</ul>
										</div>
										<div class="mdl-card__actions pt-30 pb-30">
											<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  bg-green font-white" href="#">buy now</a>
										</div>
									</div>
								</div>
								
							</div>	
						</section> -->
						<!--/Price Sec-->
						
						<!--Contact Sec-->
						<section id="contact_sec" class="contact-sec sec-pad-top-sm">
							<h2 class="mb-35">contact</h2>
							<div class="row">
								<div id="form_card_height" class="col-sm-7 mb-30">
									<div  class="mdl-card mdl-shadow--2dp" data-ng-controller="ContactController">
										<h4 class="mb-10 font-unsetcase">Hi <span  data-ng-bind="formData.inputName||'there'">there</span>, happy to hear from you.</h4>
										<form data-ng-submit="submit(contactform, $event)"  name="contactform"  method="post" class=" form-horizontal mb-30" role="form">
											<div class="form-group" data-ng-class="{ 'has-error': contactform.inputName.$invalid && submitted }">
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
													<input autocomplete="off" data-ng-model="formData.inputName" class="mdl-textfield__input" type="text" id="inputName" name="nama" required>
													<label class="mdl-textfield__label" for="inputName">name*</label>
												</div>	
											</div>
											<div class="form-group" data-ng-class="{ 'has-error': contactform.inputEmail.$invalid && submitted }">
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
													<input autocomplete="off" data-ng-model="formData.inputEmail" class="mdl-textfield__input" type="email" id="inputEmail" name="email" required>
													<label class="mdl-textfield__label" for="inputEmail">email*</label>
												</div>
											</div>
											<div class="form-group" data-ng-class="{ 'has-error': contactform.inputMessage.$invalid && submitted }">
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
													<textarea 
													data-ng-model="formData.inputMessage" 
													class="mdl-textfield__input"  rows="3" id="inputMessage" name="pesan" required></textarea>
													<label class="mdl-textfield__label" for="inputMessage">message*</label>
												</div>	
											</div>
											<div class="form-group">
												<div class="align-center">
													<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  bg-blue font-white" data-ng-disabled="submitButtonDisabled">
														submit
													</button>
												</div>
											</div>
										</form>
										<p class="block result" data-ng-class="result">{{ resultMessage }}</p>
									</div>
								</div>
								<div class="col-sm-5 mb-30">
									<div class="mdl-card mdl-shadow--2dp pa-0">
										<div id="map_canvas"></div>
									</div>
								</div>
							</div>
						</section>
						<!--/Contact Sec-->
						
						<!--Footer Sec-->
						<footer class="footer-sec sec-pad-top-sm sec-pad-bottom text-center">
							<h4>Hi, thank you for visiting.</h4>
							<p class="mt-10">Template by Hencework. Developed by Ari.</p>
							<ul class="social-icons mt-10">
								<li>
									<a class="facebook-link" href="https://web.facebook.com/sayaadhari">
										<i id="tt6" class="zmdi zmdi-facebook" target="_blank"></i>
										<div class="mdl-tooltip" data-mdl-for="tt6">
											facebook
										</div>
									</a>
								</li>
								<li>
									<a class="twitter-link" href="https://twitter.com/ariganesha">
										<i id="tt7" class="zmdi zmdi-twitter" target="_blank"></i>
										<div class="mdl-tooltip" data-mdl-for="tt7">
											twitter
										</div>
									</a>
								</li>
								<li>
									<a class="linkedin-link" href="https://www.linkedin.com/in/ariadhari/" target="_blank">
										<i id="tt8" class="zmdi zmdi-linkedin"></i>
										<div class="mdl-tooltip" data-mdl-for="tt8">
											linkedin
										</div>
									</a>
								</li>
								<li>
									<a class="instagram-link" href="https://www.instagram.com/adhariarii/" target="_blank">
										<i id="tt10" class="zmdi zmdi-instagram"></i>
										<div class="mdl-tooltip" data-mdl-for="tt10">
											instagram
										</div>
									</a>
								</li>
							</ul>

						</footer>
						<!--Footer Sec-->
					</div>
				</div>	
				<!--/Main Content-->

			</div>	
		</div>	
		<!--/Main Wrapper-->
		
		<!-- Scripts -->
		<script src="js/jquery-1.12.4.min.js"></script>
		<script src="js/angular.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/material.min.js"></script>
		<script src="js/jQuery.appear.js"></script>
		<script src="js/app.js"></script>
		<script src="js/controllers.js"></script>
		<script src="js/smooth-scroll.js"></script>
		<script src="js/isotope.js"></script>
		<script src="js/lightgallery-all.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/froogaloop2.min.js"></script>
		<script src="js/jquery.slimscroll.js"></script>
		<script>
function initMap() {
  // The location of Uluru
  var ari = {lat: -6.3557286, lng: 106.8426056};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map_canvas'), {zoom: 11, center: ari});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: ari, map: map});
}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9bdADCIuSnEWSVxByVCpOTy9BmcsiYtQ&callback=initMap"
    async defer></script>
		<script src="js/init.js"></script>
	</body>
	</html>